let getDate = unixDate => {
  let d = new Date(unixDate * 1000);
  return `${d.getDay()}/${d.getMonth()}/${d.getFullYear()}`;
}

let createJokeDiv = joke => {
  let row = document.createElement('div');
  row.className = 'row';
  row.innerHTML = `
  <div class="col s12">
      <div class="card blue-grey darken-1">
        <div class="card-content white-text">
          <span class="card-title">Joke n° ${joke.id}</span>
          <p>${joke.fact}</p>
        </div>
        <div class="card-action">
        <a>${joke.points} points</a>
        <a class="right">${getDate(joke.date)}</a>
      </div>
    </div>
  </div>`;
  document.getElementsByClassName('container')[0].appendChild(row);
}

let getJokes = () => {
  fetch('./api/jokes.json')
  .then(res => res.json()
                  .then(jokes => jokes.forEach(joke => createJokeDiv(joke))))
  .catch( error => console.error(error));
};

(() => {
  getJokes();
})();